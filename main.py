import datetime
import os
import time
import nmap
import pandas as pd
import numpy
import csv
import argparse

startTime = time.time()  # 시작 시간 기록
nm = nmap.PortScanner()  # nmap 객체 생성

# 스캔 정보
#hos = input("IP 대역을 입력 하세요. (1-254검사 / ex:172.20.8.0/24 => 172.20.8.0만 작성)\n==>")
hos = '172.20.80.177'
hostList = [hos]  # 호스트 리스트 작성
# hostList = [input("IP주소를 입력하세요(ex: 127.0.0.1) :")]  # 호스트 리스트 작성
portList = '21-23,135-140,443,445,3389'  # 포트 리스트 작성
myArg = '-sT -sU -T4 -O --open -D 1.1.1.1,2.2.2.2,3.3.3.3'  # 옵션 설정
fileList = []  # 파일 이름 저장할 리스트 생성
dirName = 'scan_result'  # 폴더 이름 지정
print("스캔 포트 ==> ", portList)

def make_folder() : # 결과 폴더 생성
    if not os.path.exists('./' + dirName):  # 폴더 없을 시 폴더 생성
        os.system('mkdir ' + dirName)
        print(dirName + " directory is made\n")
    else:  # 폴더 존재 시 기존 폴더 / 생성x
        print(dirName + ' directory exists already\n')

    if not os.path.isdir(dirName):  # 해당 파일이 폴더가 아닐 경우 오류 발생
        print("Err: Same name file exists with directory name")

# 스캔 시작
for host in hostList:
    print(f"scan {host} ...")
    print("----------------------------------------------------------\n")
    #result = nm.scan(host, portList, myArg)  # 스캔 수행
    result = nm.scan(host, '8000,22', '-sT, -O, -D 1.1.1.1,2.2.2.2,3.3.3.3')
    print(result)
    #df = pd.DataFrame(result['scan'])  # 정리를 위해 pandas 모듈 사용
    #print(df)

    # csv 저장을 위한 리스트
    fields = ['IP', 'TCP', 'UDP']
    report = []

    all: str = "" # 전체 리스트 문자열로 저장
    cnt = 1 #숫자 세기 (count)
    for i in df.keys():
        line = []
        print("IP => ", i)  # 값 확인
        line.extend([cnt, str(i), " "])
        all = all + str(i) + ", "
        try:                             # open포트 존재 시
            #TCP 결과 값
            df = pd.DataFrame(result['scan'][i]['osmatch'])
            val = df.values
            val1 = df.values
            rlt = dict(zip(df.keys(), val))
            print("TCP => ", rlt)
            line.extend([str(rlt), " "])
            all = all + str(rlt) + ", "


        except KeyError:                  # open포트가 존재 하지 않고 모두 닫힌 경우
            print("* 열린 포트 없음 \n\n", end='')
            line.extend(["", ""])
            all = all + "\n"



        cnt = cnt + 1
        report.append(line)


# 자료 가공
all = all.replace('\'', '')
all = all.replace('\"', '')
all = all.replace('{', '')
all = all.replace('}', '')



#print(all)
all_count = all.count('open')
fil_count = all.count('filtered')
fil_count = all.count('filtered')
all = all+ "\n\n"
all = all
#print(all)


# 열린포트 개수
all_count = all.count('open')
fil_count = all.count('filtered')
print("스캔한 IP 수 : ", cnt-1)
print("오픈 포트 개수(합) : ", all_count)
print("필터링 개수 : ", fil_count)
print("필터 없이 열린 포트 : ", all_count-fil_count)



# 파일 저장
'''
now = datetime.datetime.now()
ntime = str(now.strftime('%Y-%m-%d')).replace(':', '_')  # 날짜 기록
ntime = ntime.replace(' ', '_')
filename: str = ntime + '_[' + hos + ']_scan.csv'  # 파일 이름 생성
with open('C:/Users/WM-BL000098/PycharmProjects/Tool/dist/scan_result' + filename, 'w', newline='') as f:
    write = csv.writer(f)
    write.writerow(fields)
    f.write(all)
fileList.append(filename)  # 파일 리스트에 목록 추가

'''
# 점검 끝 / 결과 값 출력
print("\n----------------------------------------------------------")
print("                    점검이 종료되었습니다.")
endTime = time.time()  # 종료 시간 기록

print("----------------------------------------------------------")
print("executed Time : " + str(endTime - startTime))  # 실행 시간 출력

print("\n>>>>>>>>>>> please check your result files")
print("This is your path:\n\t" + os.path.realpath(dirName))
print("----------------------------------------------------------")
for fileName in fileList:  # 생성한 파일 목록 출력
    print(fileName)


